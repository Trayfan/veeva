﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Veeva
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public string businesses2;
        public string persons2;
        public Dictionary<string, string> links = new Dictionary<string, string>();
        private void SaveToFile(string filename, string content)
        {
            System.IO.File.WriteAllText(filename, content);
        }

        private string GetBusinesses()
        {
            using (StreamReader sr = new StreamReader("files/businessaccount.csv"))
            {
                // Read the stream to a string, and write the string to the console.
                String line = sr.ReadToEnd();
                return line;
            }
        }
        private string GetPersons()
        {
            using (StreamReader sr = new StreamReader("files/personaccount.csv"))
            {
                // Read the stream to a string, and write the string to the console.
                String line = sr.ReadToEnd();
                return line;
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            int personCount = 0;
            int businessCount = 0;
            string personText = "";
            string businessText = "";
            Random random = new Random();
            try
            {
                personText = textBox1.Text;
                businessText = textBox2.Text;
                if (personText != "")
                {
                    personCount = Int32.Parse(textBox1.Text);
                }
                if (businessText != "")
                {
                    businessCount = Int32.Parse(textBox2.Text);
                }
            }
            catch
            {
                MessageBox.Show("Count should be integer");
            }

            if (personCount > 0)
            {
                int personInd = 1;
                List<string> persons = new List<string> { };
                while (personInd <= personCount)
                {
                    
                    personaccount person = new personaccount(x: personInd, y: random);
                    person.FillValues();
                    persons.Add(person.ReturnValues());
                    personInd++;
                }
                string fileContent = "REG_Account_Status__c,REG_AMA_No_Contact__c,Do_Not_Call_vod__c,PDRP_Opt_Out_vod__c,pdrp_opt_out_date_vod__c,REG_CCT_ID__c,REG_Credential_Type__c,Credentials_vod__c,REG_EI_ID__c,Fax,FirstName,Salutation,ID,REG_IMS_ID__c,LastName,Middle_vod__c,Phone,Specialty_1_vod__c,RecordTypeID,REG_RegID__c,REG_Suffix__c,REG_Title__c,Website,REG_Infusions__c,REG_of_Infusions_in_Their_Practice__c,REG_of_Patients_on_a_Generic_Biosimilar__c,REG_of_Patients_on_Biologic_Agents__c,REG_Competition_Speaker__c,REG_Regeneron_Speaker__c,REG_Role__c,Gender_vod__c,Preferred_Name_vod__c,PersonMobilePhone,Primary_Parent_vod__c,REG_Verification_Status__c,REG_Title_2__c,REG_Title_3__c,REG_Created_By_Profile_Name__c,REG_Email__c,REG_Expert__c,REG_Last_Modified_by_Profile__c,NPI_vod__c,REG_MedPro_First_Name__c,REG_MedPro_Last_Name__c,REG_MedPro_Designation__c,REG_DUPI_Attend_Speaker_Program__c,REG_DUPI_Part_of_National_Group__c,REG_DUPI_Expert__c,REG_DUPI_of_Patients_on_Biologic_Agents__c,REG_DERMATOLOGY_Segment__c,REG_DERMATOLOGY_Target__c,REG_Associated_Products__c,REG_RHEUMATOLOGY_Target__c,REG_RHEUMATOLOGY_Tier__c,REG_CARDIO_METABOLIC_Tier__c,REG_OPHTHALMOLOGY_Tier__c,REG_IMMUNOLOGY_Tier__c,REG_CARDIO_METABOLIC_Target__c,REG_OPHTHALMOLOGY_Target__c,REG_IMMUNOLOGY_Target__c,REG_RESPIRATORY_Tier__c,REG_PAIN_Tier__c,REG_ONCOLOGY_Tier__c,REG_TA4_Tier__c,REG_TA5_Tier__c,REG_RESPIRATORY_Target__c,REG_PAIN_Target__c,REG_ONCOLOGY_Target__c,REG_TA4_Target__c,REG_TA5_Target__c,REG_VEEVAEXT__c,REG_DERMATOLOGY_Opportunity_Ranking__c,EYLEA_DMS_Target__c,REG_VEGF_Injector__c,REG_NP_Target__c,REG_NP_Tier__c";
                fileContent += Environment.NewLine;
                foreach (string x in persons)
                {
                    fileContent += x + Environment.NewLine;
                };
                SaveToFile(filename: "files/personaccount.csv", content: fileContent);
            }
            if (businessCount > 0)
            {
                int businessInd = 1;
                List<string> businesses = new List<string> { };
                while (businessInd <= businessCount)
                {

                    businessaccount business = new businessaccount(x: businessInd, y: random);
                    business.FillValues();
                    businesses.Add(business.ReturnValues());
                    businessInd++;
                }
                string fileContent = "Account_Type__c,REG_Account_Status__c,Do_Not_Call_vod__c,Name,REG_CCT_ID__c,REG_Class_of_Trade_Code__c,REG_Class_of_Trade__c,REG_CMS_Teaching_Hospital__c,REG_EI_ID__c,fax,Hospital_Type_vod__c,ID,REG_IMS_ID__c,phone,RecordTypeID,REG_RegID__c,Website,REG_Co_Pay_Assistance__c,REG_Hub_Service__c,REG_Lunches__c,REG_Payer_1__c,REG_Payer_2__c,REG_Payer_3__c,REG_Injection_Training_Lead__c,REG_Insurance_Prior_Authorizations_Lead__c,REG_Patient_Follow_Up_Lead__c,REG_Access_Lead_Contact__c,REG_Preferred_Specialty_Pharmacies__c,REG_Infusions__c,REG_of_Infusions_in_Their_Practice__c,REG_Credentialing_Required__c,REG_Credentialing_Service__c,REG_Demographics__c,REG_Disease_Awareness__c,REG_Office_Manager__c,REG_Patient_Education_Needs_Lead__c,REG_Preferred_SPP_Reason__c,REG_Receptionist__c,REG_Role__c,REG_State_Payer__c,Primary_Parent_vod__c,REG_340b__c,REG_Sample_Lead__c,REG_Allows_Samples__c,REG_Verification_Status__c,REG_Sample_Lead_Email__c,REG_Sample_Lead_Fax__c,REG_Sample_Lead_Phone__c,REG_Appeals_Lead__c,REG_Other_Preferred_Specialty_Pharmacies__c,REG_Other_Payer__c,REG_Other_Payer_2__c,REG_Created_By_Profile_Name__c,REG_Promotional_Material_Allowed__c,REG_EMR__c,REG_Email__c,REG_Last_Modified_by_Profile__c,REG_DUPI_Allows_Samples__c,REG_DUPI_Lunches__c,REG_DUPI_Disease_Awareness__c,REG_DUPI_Co_Pay_Assistance__c,REG_DUPI_Promotional_Material_Allowed__c,REG_DERM_Credentialing_Required__c,REG_DERM_Credentialing_Service__c,REG_DUPI_Demographics__c,REG_DERMATOLOGY_Segment__c,REG_DERMATOLOGY_Target__c,REG_Associated_Products__c,REG_RHEUMATOLOGY_Target__c,REG_RHEUMATOLOGY_Tier__c,REG_CARDIO_METABOLIC_Tier__c,REG_OPHTHALMOLOGY_Tier__c,REG_IMMUNOLOGY_Tier__c,REG_CARDIO_METABOLIC_Target__c,REG_OPHTHALMOLOGY_Target__c,REG_IMMUNOLOGY_Target__c,REG_RESPIRATORY_Tier__c,REG_PAIN_Tier__c,REG_ONCOLOGY_Tier__c,REG_TA4_Tier__c,REG_TA5_Tier__c,REG_RESPIRATORY_Target__c,REG_PAIN_Target__c,REG_ONCOLOGY_Target__c,REG_TA4_Target__c,REG_TA5_Target__c,reg_veevaext__c,REG_Office_Champion__c,REG_DERM_Triage_Process__c,REG_DERM_Preferred_Specialty_Pharmacies__c,REG_DERM_Other_Preferred_Specialty_Pharm__c,REG_DERM_Receptionist__c,REG_DERM_Office_Manager__c,REG_DERM_Insurance_Prior_Auth_Lead__c,REG_DERM_Injection_Training_Lead__c,REG_DERM_Patient_FollowUp_CoPayCard_Lead__c,REG_DERM_Patient_Education_Lead__c,REG_DERMATOLOGY_EMR__c,REG_DERM_Billing_Director__c";
                fileContent += Environment.NewLine;
                foreach (string x in businesses)
                {
                    fileContent += x + Environment.NewLine;
                };
                SaveToFile(filename: "files/businessaccount.csv", content: fileContent);
            }

        }

        private void TabPage2_Enter(object sender, EventArgs e)
        {
            businesses2 = GetBusinesses();
            persons2 = GetPersons();
            radioButton1.Checked = true;
        }

        private void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                DataTable dt = new DataTable();
                foreach (string columnName in "Account_Type__c,REG_Account_Status__c,Do_Not_Call_vod__c,Name,REG_CCT_ID__c,REG_Class_of_Trade_Code__c,REG_Class_of_Trade__c,REG_CMS_Teaching_Hospital__c,REG_EI_ID__c,fax,Hospital_Type_vod__c,ID,REG_IMS_ID__c,phone,RecordTypeID,REG_RegID__c,Website,REG_Co_Pay_Assistance__c,REG_Hub_Service__c,REG_Lunches__c,REG_Payer_1__c,REG_Payer_2__c,REG_Payer_3__c,REG_Injection_Training_Lead__c,REG_Insurance_Prior_Authorizations_Lead__c,REG_Patient_Follow_Up_Lead__c,REG_Access_Lead_Contact__c,REG_Preferred_Specialty_Pharmacies__c,REG_Infusions__c,REG_of_Infusions_in_Their_Practice__c,REG_Credentialing_Required__c,REG_Credentialing_Service__c,REG_Demographics__c,REG_Disease_Awareness__c,REG_Office_Manager__c,REG_Patient_Education_Needs_Lead__c,REG_Preferred_SPP_Reason__c,REG_Receptionist__c,REG_Role__c,REG_State_Payer__c,Primary_Parent_vod__c,REG_340b__c,REG_Sample_Lead__c,REG_Allows_Samples__c,REG_Verification_Status__c,REG_Sample_Lead_Email__c,REG_Sample_Lead_Fax__c,REG_Sample_Lead_Phone__c,REG_Appeals_Lead__c,REG_Other_Preferred_Specialty_Pharmacies__c,REG_Other_Payer__c,REG_Other_Payer_2__c,REG_Created_By_Profile_Name__c,REG_Promotional_Material_Allowed__c,REG_EMR__c,REG_Email__c,REG_Last_Modified_by_Profile__c,REG_DUPI_Allows_Samples__c,REG_DUPI_Lunches__c,REG_DUPI_Disease_Awareness__c,REG_DUPI_Co_Pay_Assistance__c,REG_DUPI_Promotional_Material_Allowed__c,REG_DERM_Credentialing_Required__c,REG_DERM_Credentialing_Service__c,REG_DUPI_Demographics__c,REG_DERMATOLOGY_Segment__c,REG_DERMATOLOGY_Target__c,REG_Associated_Products__c,REG_RHEUMATOLOGY_Target__c,REG_RHEUMATOLOGY_Tier__c,REG_CARDIO_METABOLIC_Tier__c,REG_OPHTHALMOLOGY_Tier__c,REG_IMMUNOLOGY_Tier__c,REG_CARDIO_METABOLIC_Target__c,REG_OPHTHALMOLOGY_Target__c,REG_IMMUNOLOGY_Target__c,REG_RESPIRATORY_Tier__c,REG_PAIN_Tier__c,REG_ONCOLOGY_Tier__c,REG_TA4_Tier__c,REG_TA5_Tier__c,REG_RESPIRATORY_Target__c,REG_PAIN_Target__c,REG_ONCOLOGY_Target__c,REG_TA4_Target__c,REG_TA5_Target__c,reg_veevaext__c,REG_Office_Champion__c,REG_DERM_Triage_Process__c,REG_DERM_Preferred_Specialty_Pharmacies__c,REG_DERM_Other_Preferred_Specialty_Pharm__c,REG_DERM_Receptionist__c,REG_DERM_Office_Manager__c,REG_DERM_Insurance_Prior_Auth_Lead__c,REG_DERM_Injection_Training_Lead__c,REG_DERM_Patient_FollowUp_CoPayCard_Lead__c,REG_DERM_Patient_Education_Lead__c,REG_DERMATOLOGY_EMR__c,REG_DERM_Billing_Director__c".Split(','))
                {
                    DataColumn dc = new DataColumn(columnName, typeof(String));
                    dt.Columns.Add(dc);
                }
                int businessCount = businesses2.Split('\n').Count()-1;
                int x = 1;
                while (x < businessCount)
                {
                    DataRow dr = dt.NewRow();
                    int y = 0;
                    while (y < businesses2.Split('\n')[x].Split(',').Count()-1)
                    {
                        dr[y] = businesses2.Split('\n')[x].Split(',')[y];
                        y++;
                    }
                    dt.Rows.Add(dr);
                    x++;
                }
                comboBox1.DataSource = dt;
                comboBox1.DisplayMember = "ID";


                DataTable dt2 = new DataTable();
                foreach (string columnName in "REG_Account_Status__c,REG_AMA_No_Contact__c,Do_Not_Call_vod__c,PDRP_Opt_Out_vod__c,pdrp_opt_out_date_vod__c,REG_CCT_ID__c,REG_Credential_Type__c,Credentials_vod__c,REG_EI_ID__c,Fax,FirstName,Salutation,ID,REG_IMS_ID__c,LastName,Middle_vod__c,Phone,Specialty_1_vod__c,RecordTypeID,REG_RegID__c,REG_Suffix__c,REG_Title__c,Website,REG_Infusions__c,REG_of_Infusions_in_Their_Practice__c,REG_of_Patients_on_a_Generic_Biosimilar__c,REG_of_Patients_on_Biologic_Agents__c,REG_Competition_Speaker__c,REG_Regeneron_Speaker__c,REG_Role__c,Gender_vod__c,Preferred_Name_vod__c,PersonMobilePhone,Primary_Parent_vod__c,REG_Verification_Status__c,REG_Title_2__c,REG_Title_3__c,REG_Created_By_Profile_Name__c,REG_Email__c,REG_Expert__c,REG_Last_Modified_by_Profile__c,NPI_vod__c,REG_MedPro_First_Name__c,REG_MedPro_Last_Name__c,REG_MedPro_Designation__c,REG_DUPI_Attend_Speaker_Program__c,REG_DUPI_Part_of_National_Group__c,REG_DUPI_Expert__c,REG_DUPI_of_Patients_on_Biologic_Agents__c,REG_DERMATOLOGY_Segment__c,REG_DERMATOLOGY_Target__c,REG_Associated_Products__c,REG_RHEUMATOLOGY_Target__c,REG_RHEUMATOLOGY_Tier__c,REG_CARDIO_METABOLIC_Tier__c,REG_OPHTHALMOLOGY_Tier__c,REG_IMMUNOLOGY_Tier__c,REG_CARDIO_METABOLIC_Target__c,REG_OPHTHALMOLOGY_Target__c,REG_IMMUNOLOGY_Target__c,REG_RESPIRATORY_Tier__c,REG_PAIN_Tier__c,REG_ONCOLOGY_Tier__c,REG_TA4_Tier__c,REG_TA5_Tier__c,REG_RESPIRATORY_Target__c,REG_PAIN_Target__c,REG_ONCOLOGY_Target__c,REG_TA4_Target__c,REG_TA5_Target__c,REG_VEEVAEXT__c,REG_DERMATOLOGY_Opportunity_Ranking__c,EYLEA_DMS_Target__c,REG_VEGF_Injector__c,REG_NP_Target__c,REG_NP_Tier__c".Split(','))
                {
                    DataColumn dc2 = new DataColumn(columnName, typeof(String));
                    dt2.Columns.Add(dc2);
                }
                int personsCount = persons2.Split('\n').Count() - 1;
                int x2 = 1;
                while (x2 < personsCount)
                {
                    DataRow dr2 = dt2.NewRow();
                    int y2 = 0;
                    while (y2 < persons2.Split('\n')[x2].Split(',').Count() - 1)
                    {
                        dr2[y2] = persons2.Split('\n')[x2].Split(',')[y2];
                        y2++;
                    }
                    dt2.Rows.Add(dr2);
                    x2++;
                }
                checkedListBox2.DataSource = dt2;
                checkedListBox2.DisplayMember = "ID";
            }
            if (radioButton2.Checked)
            {
                DataTable dt = new DataTable();
                foreach (string columnName in "Account_Type__c,REG_Account_Status__c,Do_Not_Call_vod__c,Name,REG_CCT_ID__c,REG_Class_of_Trade_Code__c,REG_Class_of_Trade__c,REG_CMS_Teaching_Hospital__c,REG_EI_ID__c,fax,Hospital_Type_vod__c,ID,REG_IMS_ID__c,phone,RecordTypeID,REG_RegID__c,Website,REG_Co_Pay_Assistance__c,REG_Hub_Service__c,REG_Lunches__c,REG_Payer_1__c,REG_Payer_2__c,REG_Payer_3__c,REG_Injection_Training_Lead__c,REG_Insurance_Prior_Authorizations_Lead__c,REG_Patient_Follow_Up_Lead__c,REG_Access_Lead_Contact__c,REG_Preferred_Specialty_Pharmacies__c,REG_Infusions__c,REG_of_Infusions_in_Their_Practice__c,REG_Credentialing_Required__c,REG_Credentialing_Service__c,REG_Demographics__c,REG_Disease_Awareness__c,REG_Office_Manager__c,REG_Patient_Education_Needs_Lead__c,REG_Preferred_SPP_Reason__c,REG_Receptionist__c,REG_Role__c,REG_State_Payer__c,Primary_Parent_vod__c,REG_340b__c,REG_Sample_Lead__c,REG_Allows_Samples__c,REG_Verification_Status__c,REG_Sample_Lead_Email__c,REG_Sample_Lead_Fax__c,REG_Sample_Lead_Phone__c,REG_Appeals_Lead__c,REG_Other_Preferred_Specialty_Pharmacies__c,REG_Other_Payer__c,REG_Other_Payer_2__c,REG_Created_By_Profile_Name__c,REG_Promotional_Material_Allowed__c,REG_EMR__c,REG_Email__c,REG_Last_Modified_by_Profile__c,REG_DUPI_Allows_Samples__c,REG_DUPI_Lunches__c,REG_DUPI_Disease_Awareness__c,REG_DUPI_Co_Pay_Assistance__c,REG_DUPI_Promotional_Material_Allowed__c,REG_DERM_Credentialing_Required__c,REG_DERM_Credentialing_Service__c,REG_DUPI_Demographics__c,REG_DERMATOLOGY_Segment__c,REG_DERMATOLOGY_Target__c,REG_Associated_Products__c,REG_RHEUMATOLOGY_Target__c,REG_RHEUMATOLOGY_Tier__c,REG_CARDIO_METABOLIC_Tier__c,REG_OPHTHALMOLOGY_Tier__c,REG_IMMUNOLOGY_Tier__c,REG_CARDIO_METABOLIC_Target__c,REG_OPHTHALMOLOGY_Target__c,REG_IMMUNOLOGY_Target__c,REG_RESPIRATORY_Tier__c,REG_PAIN_Tier__c,REG_ONCOLOGY_Tier__c,REG_TA4_Tier__c,REG_TA5_Tier__c,REG_RESPIRATORY_Target__c,REG_PAIN_Target__c,REG_ONCOLOGY_Target__c,REG_TA4_Target__c,REG_TA5_Target__c,reg_veevaext__c,REG_Office_Champion__c,REG_DERM_Triage_Process__c,REG_DERM_Preferred_Specialty_Pharmacies__c,REG_DERM_Other_Preferred_Specialty_Pharm__c,REG_DERM_Receptionist__c,REG_DERM_Office_Manager__c,REG_DERM_Insurance_Prior_Auth_Lead__c,REG_DERM_Injection_Training_Lead__c,REG_DERM_Patient_FollowUp_CoPayCard_Lead__c,REG_DERM_Patient_Education_Lead__c,REG_DERMATOLOGY_EMR__c,REG_DERM_Billing_Director__c".Split(','))
                {
                    DataColumn dc = new DataColumn(columnName, typeof(String));
                    dt.Columns.Add(dc);
                }
                int businessCount = businesses2.Split('\n').Count() - 1;
                int x = 1;
                while (x < businessCount)
                {
                    DataRow dr = dt.NewRow();
                    int y = 0;
                    while (y < businesses2.Split('\n')[x].Split(',').Count() - 1)
                    {
                        dr[y] = businesses2.Split('\n')[x].Split(',')[y];
                        y++;
                    }
                    dt.Rows.Add(dr);
                    x++;
                }
                comboBox1.DataSource = dt;
                comboBox1.DisplayMember = "ID";


                DataTable dt2 = new DataTable();
                foreach (string columnName in "Account_Type__c,REG_Account_Status__c,Do_Not_Call_vod__c,Name,REG_CCT_ID__c,REG_Class_of_Trade_Code__c,REG_Class_of_Trade__c,REG_CMS_Teaching_Hospital__c,REG_EI_ID__c,fax,Hospital_Type_vod__c,ID,REG_IMS_ID__c,phone,RecordTypeID,REG_RegID__c,Website,REG_Co_Pay_Assistance__c,REG_Hub_Service__c,REG_Lunches__c,REG_Payer_1__c,REG_Payer_2__c,REG_Payer_3__c,REG_Injection_Training_Lead__c,REG_Insurance_Prior_Authorizations_Lead__c,REG_Patient_Follow_Up_Lead__c,REG_Access_Lead_Contact__c,REG_Preferred_Specialty_Pharmacies__c,REG_Infusions__c,REG_of_Infusions_in_Their_Practice__c,REG_Credentialing_Required__c,REG_Credentialing_Service__c,REG_Demographics__c,REG_Disease_Awareness__c,REG_Office_Manager__c,REG_Patient_Education_Needs_Lead__c,REG_Preferred_SPP_Reason__c,REG_Receptionist__c,REG_Role__c,REG_State_Payer__c,Primary_Parent_vod__c,REG_340b__c,REG_Sample_Lead__c,REG_Allows_Samples__c,REG_Verification_Status__c,REG_Sample_Lead_Email__c,REG_Sample_Lead_Fax__c,REG_Sample_Lead_Phone__c,REG_Appeals_Lead__c,REG_Other_Preferred_Specialty_Pharmacies__c,REG_Other_Payer__c,REG_Other_Payer_2__c,REG_Created_By_Profile_Name__c,REG_Promotional_Material_Allowed__c,REG_EMR__c,REG_Email__c,REG_Last_Modified_by_Profile__c,REG_DUPI_Allows_Samples__c,REG_DUPI_Lunches__c,REG_DUPI_Disease_Awareness__c,REG_DUPI_Co_Pay_Assistance__c,REG_DUPI_Promotional_Material_Allowed__c,REG_DERM_Credentialing_Required__c,REG_DERM_Credentialing_Service__c,REG_DUPI_Demographics__c,REG_DERMATOLOGY_Segment__c,REG_DERMATOLOGY_Target__c,REG_Associated_Products__c,REG_RHEUMATOLOGY_Target__c,REG_RHEUMATOLOGY_Tier__c,REG_CARDIO_METABOLIC_Tier__c,REG_OPHTHALMOLOGY_Tier__c,REG_IMMUNOLOGY_Tier__c,REG_CARDIO_METABOLIC_Target__c,REG_OPHTHALMOLOGY_Target__c,REG_IMMUNOLOGY_Target__c,REG_RESPIRATORY_Tier__c,REG_PAIN_Tier__c,REG_ONCOLOGY_Tier__c,REG_TA4_Tier__c,REG_TA5_Tier__c,REG_RESPIRATORY_Target__c,REG_PAIN_Target__c,REG_ONCOLOGY_Target__c,REG_TA4_Target__c,REG_TA5_Target__c,reg_veevaext__c,REG_Office_Champion__c,REG_DERM_Triage_Process__c,REG_DERM_Preferred_Specialty_Pharmacies__c,REG_DERM_Other_Preferred_Specialty_Pharm__c,REG_DERM_Receptionist__c,REG_DERM_Office_Manager__c,REG_DERM_Insurance_Prior_Auth_Lead__c,REG_DERM_Injection_Training_Lead__c,REG_DERM_Patient_FollowUp_CoPayCard_Lead__c,REG_DERM_Patient_Education_Lead__c,REG_DERMATOLOGY_EMR__c,REG_DERM_Billing_Director__c".Split(','))
                {
                    DataColumn dc2 = new DataColumn(columnName, typeof(String));
                    dt2.Columns.Add(dc2);
                }
                businessCount = businesses2.Split('\n').Count() - 1;
                int x2 = 1;
                while (x2 < businessCount)
                {
                    DataRow dr2 = dt2.NewRow();
                    int y2 = 0;
                    while (y2 < businesses2.Split('\n')[x2].Split(',').Count() - 1)
                    {
                        dr2[y2] = businesses2.Split('\n')[x2].Split(',')[y2];
                        y2++;
                    }
                    dt2.Rows.Add(dr2);
                    x2++;
                }
                checkedListBox2.DataSource = dt2;
                checkedListBox2.DisplayMember = "ID";
            }
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // если b2b - убрать выбранное из child
            // выделить всех, кого выделили до этого
            // сохранение выделения происходит по нажатию по кнопке
            if (radioButton2.Checked)
            {
                int i = 0;
            }

        }
    }
}
