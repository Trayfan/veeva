﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Veeva
{
    class businessaccount
    {
        public Random random;
        public int indid;
        public string Account_Type__c;
        public string ID;
        public string Name;
        public string RecordTypeID;
        public string REG_CCT_ID__c;
        public string REG_EI_ID__c;
        public string REG_IMS_ID__c;
        public string REG_RegID__c;
        public string fax;
        public string phone;
        public string REG_Email__c;
        public string Website;
        public string Do_Not_Call_vod__c;
        public string Hospital_Type_vod__c;
        public string Primary_Parent_vod__c;
        public string REG_340b__c;
        public string REG_Access_Lead_Contact__c;
        public string REG_Account_Status__c;
        public string REG_Allows_Samples__c;
        public string REG_Appeals_Lead__c;
        public string REG_Associated_Products__c;
        public string REG_CARDIO_METABOLIC_Target__c;
        public string REG_CARDIO_METABOLIC_Tier__c;
        public string REG_Class_of_Trade__c;
        public string REG_Class_of_Trade_Code__c;
        public string REG_CMS_Teaching_Hospital__c;
        public string REG_Co_Pay_Assistance__c;
        public string REG_Created_By_Profile_Name__c;
        public string REG_Credentialing_Required__c;
        public string REG_Credentialing_Service__c;
        public string REG_Demographics__c;
        public string REG_DERM_Billing_Director__c;
        public string REG_DERM_Credentialing_Required__c;
        public string REG_DERM_Credentialing_Service__c;
        public string REG_DERM_Injection_Training_Lead__c;
        public string REG_DERM_Insurance_Prior_Auth_Lead__c;
        public string REG_DERM_Office_Manager__c;
        public string REG_DERM_Other_Preferred_Specialty_Pharm__c;
        public string REG_DERM_Patient_Education_Lead__c;
        public string REG_DERM_Patient_FollowUp_CoPayCard_Lead__c;
        public string REG_DERM_Preferred_Specialty_Pharmacies__c;
        public string REG_DERM_Receptionist__c;
        public string REG_DERM_Triage_Process__c;
        public string REG_DERMATOLOGY_EMR__c;
        public string REG_DERMATOLOGY_Segment__c;
        public string REG_DERMATOLOGY_Target__c;
        public string REG_Disease_Awareness__c;
        public string REG_DUPI_Allows_Samples__c;
        public string REG_DUPI_Co_Pay_Assistance__c;
        public string REG_DUPI_Demographics__c;
        public string REG_DUPI_Disease_Awareness__c;
        public string REG_DUPI_Lunches__c;
        public string REG_DUPI_Promotional_Material_Allowed__c;
        public string REG_EMR__c;
        public string REG_Hub_Service__c;
        public string REG_IMMUNOLOGY_Target__c;
        public string REG_IMMUNOLOGY_Tier__c;
        public string REG_Infusions__c;
        public string REG_Injection_Training_Lead__c;
        public string REG_Insurance_Prior_Authorizations_Lead__c;
        public string REG_Last_Modified_by_Profile__c;
        public string REG_Lunches__c;
        public string REG_of_Infusions_in_Their_Practice__c;
        public string REG_Office_Champion__c;
        public string REG_Office_Manager__c;
        public string REG_ONCOLOGY_Target__c;
        public string REG_ONCOLOGY_Tier__c;
        public string REG_OPHTHALMOLOGY_Target__c;
        public string REG_OPHTHALMOLOGY_Tier__c;
        public string REG_Other_Payer__c;
        public string REG_Other_Payer_2__c;
        public string REG_Other_Preferred_Specialty_Pharmacies__c;
        public string REG_PAIN_Target__c;
        public string REG_PAIN_Tier__c;
        public string REG_Patient_Education_Needs_Lead__c;
        public string REG_Patient_Follow_Up_Lead__c;
        public string REG_Payer_1__c;
        public string REG_Payer_2__c;
        public string REG_Payer_3__c;
        public string REG_Preferred_Specialty_Pharmacies__c;
        public string REG_Preferred_SPP_Reason__c;
        public string REG_Promotional_Material_Allowed__c;
        public string REG_Receptionist__c;
        public string REG_RESPIRATORY_Target__c;
        public string REG_RESPIRATORY_Tier__c;
        public string REG_RHEUMATOLOGY_Target__c;
        public string REG_RHEUMATOLOGY_Tier__c;
        public string REG_Role__c;
        public string REG_Sample_Lead__c;
        public string REG_Sample_Lead_Email__c;
        public string REG_Sample_Lead_Fax__c;
        public string REG_Sample_Lead_Phone__c;
        public string REG_State_Payer__c;
        public string REG_TA4_Target__c;
        public string REG_TA4_Tier__c;
        public string REG_TA5_Target__c;
        public string REG_TA5_Tier__c;
        public string reg_veevaext__c;
        public string REG_Verification_Status__c;


        public businessaccount(int x, Random y) { indid = x; random = y; }


        public void FillValues()
        {
            Account_Type__c = Enumiration.Account_Type__c[random.Next(Enumiration.Account_Type__c.Count)];
            ID = "0010e" + indid.ToString("00000") + "MbEppAAF";
            Name = Enumiration.Name[random.Next(Enumiration.Name.Count)];
            RecordTypeID = Enumiration.RecordTypeIDb[random.Next(Enumiration.RecordTypeIDb.Count)];
            REG_CCT_ID__c = indid.ToString("0000000000");
            REG_EI_ID__c = indid.ToString("0000000");
            REG_IMS_ID__c = "INS" + indid.ToString("00000000");
            REG_RegID__c = "";
            fax = random.Next(1000000000, 2147483647).ToString();
            phone = random.Next(1000000000, 2147483647).ToString();
            REG_Email__c = "mail" + random.Next(0, 1000000) + "@gmail.com";
            Website = "www.website" + random.Next(0, 1000000) + ".com";
            Do_Not_Call_vod__c = "";
            Hospital_Type_vod__c = Enumiration.hospital_type_vod__c[random.Next(Enumiration.hospital_type_vod__c.Count)];
            Primary_Parent_vod__c = "";
            REG_340b__c = "";
            REG_Access_Lead_Contact__c = "";
            REG_Account_Status__c = "Active";
            REG_Allows_Samples__c = "";
            REG_Appeals_Lead__c = "";
            REG_Associated_Products__c = "";
            REG_CARDIO_METABOLIC_Target__c = "";
            REG_CARDIO_METABOLIC_Tier__c = "";
            REG_Class_of_Trade__c = "";
            REG_Class_of_Trade_Code__c = "";
            REG_CMS_Teaching_Hospital__c = "0";
            REG_Co_Pay_Assistance__c = "";
            REG_Created_By_Profile_Name__c = "";
            REG_Credentialing_Required__c = "";
            REG_Credentialing_Service__c = "";
            REG_Demographics__c = "";
            REG_DERM_Billing_Director__c = "";
            REG_DERM_Credentialing_Required__c = "";
            REG_DERM_Credentialing_Service__c = "";
            REG_DERM_Injection_Training_Lead__c = "";
            REG_DERM_Insurance_Prior_Auth_Lead__c = "";
            REG_DERM_Office_Manager__c = "";
            REG_DERM_Other_Preferred_Specialty_Pharm__c = "";
            REG_DERM_Patient_Education_Lead__c = "";
            REG_DERM_Patient_FollowUp_CoPayCard_Lead__c = "";
            REG_DERM_Preferred_Specialty_Pharmacies__c = "";
            REG_DERM_Receptionist__c = "";
            REG_DERM_Triage_Process__c = "";
            REG_DERMATOLOGY_EMR__c = "";
            REG_DERMATOLOGY_Segment__c = "";
            REG_DERMATOLOGY_Target__c = "";
            REG_Disease_Awareness__c = "";
            REG_DUPI_Allows_Samples__c = "";
            REG_DUPI_Co_Pay_Assistance__c = "";
            REG_DUPI_Demographics__c = "";
            REG_DUPI_Disease_Awareness__c = "";
            REG_DUPI_Lunches__c = "";
            REG_DUPI_Promotional_Material_Allowed__c = "";
            REG_EMR__c = "";
            REG_Hub_Service__c = "";
            REG_IMMUNOLOGY_Target__c = "";
            REG_IMMUNOLOGY_Tier__c = "";
            REG_Infusions__c = "";
            REG_Injection_Training_Lead__c = "";
            REG_Insurance_Prior_Authorizations_Lead__c = "";
            REG_Last_Modified_by_Profile__c = "";
            REG_Lunches__c = "";
            REG_of_Infusions_in_Their_Practice__c = "";
            REG_Office_Champion__c = "";
            REG_Office_Manager__c = "";
            REG_ONCOLOGY_Target__c = "";
            REG_ONCOLOGY_Tier__c = "";
            REG_OPHTHALMOLOGY_Target__c = "";
            REG_OPHTHALMOLOGY_Tier__c = "";
            REG_Other_Payer__c = "";
            REG_Other_Payer_2__c = "";
            REG_Other_Preferred_Specialty_Pharmacies__c = "";
            REG_PAIN_Target__c = "";
            REG_PAIN_Tier__c = "";
            REG_Patient_Education_Needs_Lead__c = "";
            REG_Patient_Follow_Up_Lead__c = "";
            REG_Payer_1__c = "";
            REG_Payer_2__c = "";
            REG_Payer_3__c = "";
            REG_Preferred_Specialty_Pharmacies__c = "";
            REG_Preferred_SPP_Reason__c = "";
            REG_Promotional_Material_Allowed__c = "";
            REG_Receptionist__c = "";
            REG_RESPIRATORY_Target__c = "";
            REG_RESPIRATORY_Tier__c = "";
            REG_RHEUMATOLOGY_Target__c = "";
            REG_RHEUMATOLOGY_Tier__c = "";
            REG_Role__c = "";
            REG_Sample_Lead__c = "";
            REG_Sample_Lead_Email__c = "";
            REG_Sample_Lead_Fax__c = "";
            REG_Sample_Lead_Phone__c = "";
            REG_State_Payer__c = "";
            REG_TA4_Target__c = "";
            REG_TA4_Tier__c = "";
            REG_TA5_Target__c = "";
            REG_TA5_Tier__c = "";
            reg_veevaext__c = "";
            REG_Verification_Status__c = "";
        }

        public string ReturnValues()
        {
            List<string> totalValues = new List<string>
            {
                Account_Type__c,
                REG_Account_Status__c,
                Do_Not_Call_vod__c,
                Name,
                REG_CCT_ID__c,
                REG_Class_of_Trade_Code__c,
                REG_Class_of_Trade__c,
                REG_CMS_Teaching_Hospital__c,
                REG_EI_ID__c,
                fax,
                Hospital_Type_vod__c,
                ID,
                REG_IMS_ID__c,
                phone,
                RecordTypeID,
                REG_RegID__c,
                Website,
                REG_Co_Pay_Assistance__c,
                REG_Hub_Service__c,
                REG_Lunches__c,
                REG_Payer_1__c,
                REG_Payer_2__c,
                REG_Payer_3__c,
                REG_Injection_Training_Lead__c,
                REG_Insurance_Prior_Authorizations_Lead__c,
                REG_Patient_Follow_Up_Lead__c,
                REG_Access_Lead_Contact__c,
                REG_Preferred_Specialty_Pharmacies__c,
                REG_Infusions__c,
                REG_of_Infusions_in_Their_Practice__c,
                REG_Credentialing_Required__c,
                REG_Credentialing_Service__c,
                REG_Demographics__c,
                REG_Disease_Awareness__c,
                REG_Office_Manager__c,
                REG_Patient_Education_Needs_Lead__c,
                REG_Preferred_SPP_Reason__c,
                REG_Receptionist__c,
                REG_Role__c,
                REG_State_Payer__c,
                Primary_Parent_vod__c,
                REG_340b__c,
                REG_Sample_Lead__c,
                REG_Allows_Samples__c,
                REG_Verification_Status__c,
                REG_Sample_Lead_Email__c,
                REG_Sample_Lead_Fax__c,
                REG_Sample_Lead_Phone__c,
                REG_Appeals_Lead__c,
                REG_Other_Preferred_Specialty_Pharmacies__c,
                REG_Other_Payer__c,
                REG_Other_Payer_2__c,
                REG_Created_By_Profile_Name__c,
                REG_Promotional_Material_Allowed__c,
                REG_EMR__c,
                REG_Email__c,
                REG_Last_Modified_by_Profile__c,
                REG_DUPI_Allows_Samples__c,
                REG_DUPI_Lunches__c,
                REG_DUPI_Disease_Awareness__c,
                REG_DUPI_Co_Pay_Assistance__c,
                REG_DUPI_Promotional_Material_Allowed__c,
                REG_DERM_Credentialing_Required__c,
                REG_DERM_Credentialing_Service__c,
                REG_DUPI_Demographics__c,
                REG_DERMATOLOGY_Segment__c,
                REG_DERMATOLOGY_Target__c,
                REG_Associated_Products__c,
                REG_RHEUMATOLOGY_Target__c,
                REG_RHEUMATOLOGY_Tier__c,
                REG_CARDIO_METABOLIC_Tier__c,
                REG_OPHTHALMOLOGY_Tier__c,
                REG_IMMUNOLOGY_Tier__c,
                REG_CARDIO_METABOLIC_Target__c,
                REG_OPHTHALMOLOGY_Target__c,
                REG_IMMUNOLOGY_Target__c,
                REG_RESPIRATORY_Tier__c,
                REG_PAIN_Tier__c,
                REG_ONCOLOGY_Tier__c,
                REG_TA4_Tier__c,
                REG_TA5_Tier__c,
                REG_RESPIRATORY_Target__c,
                REG_PAIN_Target__c,
                REG_ONCOLOGY_Target__c,
                REG_TA4_Target__c,
                REG_TA5_Target__c,
                reg_veevaext__c,
                REG_Office_Champion__c,
                REG_DERM_Triage_Process__c,
                REG_DERM_Preferred_Specialty_Pharmacies__c,
                REG_DERM_Other_Preferred_Specialty_Pharm__c,
                REG_DERM_Receptionist__c,
                REG_DERM_Office_Manager__c,
                REG_DERM_Insurance_Prior_Auth_Lead__c,
                REG_DERM_Injection_Training_Lead__c,
                REG_DERM_Patient_FollowUp_CoPayCard_Lead__c,
                REG_DERM_Patient_Education_Lead__c,
                REG_DERMATOLOGY_EMR__c,
                REG_DERM_Billing_Director__c

            };
            return String.Join(",", totalValues.ToArray());
        }
    }
}
