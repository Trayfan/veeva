﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Veeva
{
    class personaccount
    {
        public Random random;
        public int indid;
        public string REG_Account_Status__c;
        public string REG_AMA_No_Contact__c;
        public string Do_Not_Call_vod__c;
        public string PDRP_Opt_Out_vod__c;
        public string pdrp_opt_out_date_vod__c;
        public string REG_CCT_ID__c;
        public string REG_Credential_Type__c;
        public string Credentials_vod__c;
        public string REG_EI_ID__c;
        public string Fax;
        public string FirstName;
        public string Salutation;
        public string ID;
        public string REG_IMS_ID__c;
        public string LastName;
        public string Middle_vod__c;
        public string Phone;
        public string Specialty_1_vod__c;
        public string RecordTypeID;
        public string REG_RegID__c;
        public string REG_Suffix__c;
        public string REG_Title__c;
        public string Website;
        public string REG_Infusions__c;
        public string REG_of_Infusions_in_Their_Practice__c;
        public string REG_of_Patients_on_a_Generic_Biosimilar__c;
        public string REG_of_Patients_on_Biologic_Agents__c;
        public string REG_Competition_Speaker__c;
        public string REG_Regeneron_Speaker__c;
        public string REG_Role__c;
        public string Gender_vod__c;
        public string Preferred_Name_vod__c;
        public string PersonMobilePhone;
        public string Primary_Parent_vod__c;
        public string REG_Verification_Status__c;
        public string REG_Title_2__c;
        public string REG_Title_3__c;
        public string REG_Created_By_Profile_Name__c;
        public string REG_Email__c;
        public string REG_Expert__c;
        public string REG_Last_Modified_by_Profile__c;
        public string NPI_vod__c;
        public string REG_MedPro_First_Name__c;
        public string REG_MedPro_Last_Name__c;
        public string REG_MedPro_Designation__c;
        public string REG_DUPI_Attend_Speaker_Program__c;
        public string REG_DUPI_Part_of_National_Group__c;
        public string REG_DUPI_Expert__c;
        public string REG_DUPI_of_Patients_on_Biologic_Agents__c;
        public string REG_DERMATOLOGY_Segment__c;
        public string REG_DERMATOLOGY_Target__c;
        public string REG_Associated_Products__c;
        public string REG_RHEUMATOLOGY_Target__c;
        public string REG_RHEUMATOLOGY_Tier__c;
        public string REG_CARDIO_METABOLIC_Tier__c;
        public string REG_OPHTHALMOLOGY_Tier__c;
        public string REG_IMMUNOLOGY_Tier__c;
        public string REG_CARDIO_METABOLIC_Target__c;
        public string REG_OPHTHALMOLOGY_Target__c;
        public string REG_IMMUNOLOGY_Target__c;
        public string REG_RESPIRATORY_Tier__c;
        public string REG_PAIN_Tier__c;
        public string REG_ONCOLOGY_Tier__c;
        public string REG_TA4_Tier__c;
        public string REG_TA5_Tier__c;
        public string REG_RESPIRATORY_Target__c;
        public string REG_PAIN_Target__c;
        public string REG_ONCOLOGY_Target__c;
        public string REG_TA4_Target__c;
        public string REG_TA5_Target__c;
        public string REG_VEEVAEXT__c;
        public string REG_DERMATOLOGY_Opportunity_Ranking__c;
        public string EYLEA_DMS_Target__c;
        public string REG_VEGF_Injector__c;
        public string REG_NP_Target__c;
        public string REG_NP_Tier__c;

        public personaccount(int x, Random y) { indid = x; random = y; }


        public void FillValues()
        {
            
            REG_Account_Status__c = "Active";
            REG_AMA_No_Contact__c = Enumiration.REG_AMA_No_Contact__c[random.Next(Enumiration.REG_AMA_No_Contact__c.Count)];
            Do_Not_Call_vod__c = Enumiration.Do_Not_Call_vod__c[random.Next(Enumiration.Do_Not_Call_vod__c.Count)];
            PDRP_Opt_Out_vod__c = "0";
            pdrp_opt_out_date_vod__c = "";
            REG_CCT_ID__c = indid.ToString("0000000000");
            REG_Credential_Type__c = Enumiration.REG_Credential_Type__c[random.Next(Enumiration.REG_Credential_Type__c.Count)];
            Credentials_vod__c = Enumiration.Credentials_vod__c[random.Next(Enumiration.Credentials_vod__c.Count)];
            REG_EI_ID__c = "";
            Fax = random.Next(1000000000, 2147483647).ToString();
            FirstName = Enumiration.FirstName[random.Next(Enumiration.FirstName.Count)];
            Salutation = Enumiration.Salutation[random.Next(Enumiration.Salutation.Count)];
            ID = "0010e" + indid.ToString("00000") + "MbGqiAAF";
            REG_IMS_ID__c = indid.ToString("0000000");
            LastName = Enumiration.LastName[random.Next(Enumiration.LastName.Count)];
            Middle_vod__c = Enumiration.Middle_vod__c[random.Next(Enumiration.Middle_vod__c.Count)];
            Phone = random.Next(1000000000, 2147483647).ToString();
            Specialty_1_vod__c = Enumiration.Specialty_1_vod__c[random.Next(Enumiration.Specialty_1_vod__c.Count)];
            RecordTypeID = Enumiration.RecordTypeIDp[random.Next(Enumiration.RecordTypeIDp.Count)];
            REG_RegID__c = "";
            REG_Suffix__c = Enumiration.REG_Suffix__c[random.Next(Enumiration.REG_Suffix__c.Count)];
            REG_Title__c = Enumiration.REG_Title__c[random.Next(Enumiration.REG_Title__c.Count)];
            Website = "www.website" + random.Next(0, 1000000) + ".com";
            REG_Infusions__c = "";
            REG_of_Infusions_in_Their_Practice__c = "";
            REG_of_Patients_on_a_Generic_Biosimilar__c = "";
            REG_of_Patients_on_Biologic_Agents__c = "";
            REG_Competition_Speaker__c = "";
            REG_Regeneron_Speaker__c = "";
            REG_Role__c = "";
            Gender_vod__c = Enumiration.Gender_vod__c[random.Next(Enumiration.Gender_vod__c.Count)];
            Preferred_Name_vod__c = FirstName;
            PersonMobilePhone = Phone;
            Primary_Parent_vod__c = "";
            REG_Verification_Status__c = "";
            REG_Title_2__c = "";
            REG_Title_3__c = "";
            REG_Created_By_Profile_Name__c = "";
            REG_Email__c = FirstName + "_" + LastName + "@gmail.com";
            REG_Expert__c = "";
            REG_Last_Modified_by_Profile__c = "";
            NPI_vod__c = indid.ToString("0000000000");
            REG_MedPro_First_Name__c = "";
            REG_MedPro_Last_Name__c = "";
            REG_MedPro_Designation__c = "";
            REG_DUPI_Attend_Speaker_Program__c = "";
            REG_DUPI_Part_of_National_Group__c = "";
            REG_DUPI_Expert__c = "";
            REG_DUPI_of_Patients_on_Biologic_Agents__c = "";
            REG_DERMATOLOGY_Segment__c = "";
            REG_DERMATOLOGY_Target__c = "";
            REG_Associated_Products__c = Enumiration.REG_Associated_Products__c[random.Next(Enumiration.REG_Associated_Products__c.Count)];
            REG_RHEUMATOLOGY_Target__c = "";
            REG_RHEUMATOLOGY_Tier__c = "";
            REG_CARDIO_METABOLIC_Tier__c = "";
            REG_OPHTHALMOLOGY_Tier__c = "";
            REG_IMMUNOLOGY_Tier__c = "";
            REG_CARDIO_METABOLIC_Target__c = "";
            REG_OPHTHALMOLOGY_Target__c = "";
            REG_IMMUNOLOGY_Target__c = "";
            REG_RESPIRATORY_Tier__c = "";
            REG_PAIN_Tier__c = "";
            REG_ONCOLOGY_Tier__c = "";
            REG_TA4_Tier__c = "";
            REG_TA5_Tier__c = "";
            REG_RESPIRATORY_Target__c = "";
            REG_PAIN_Target__c = "";
            REG_ONCOLOGY_Target__c = "";
            REG_TA4_Target__c = "";
            REG_TA5_Target__c = "";
            REG_VEEVAEXT__c = "";
            REG_DERMATOLOGY_Opportunity_Ranking__c = "";
            EYLEA_DMS_Target__c = "";
            REG_VEGF_Injector__c = "";
            REG_NP_Target__c = "";
            REG_NP_Tier__c = "";
        }

        public string ReturnValues()
        {
            List<string> totalValues = new List<string>
            {
                REG_Account_Status__c,
                REG_AMA_No_Contact__c,
                Do_Not_Call_vod__c,
                PDRP_Opt_Out_vod__c,
                pdrp_opt_out_date_vod__c,
                REG_CCT_ID__c,
                REG_Credential_Type__c,
                Credentials_vod__c,
                REG_EI_ID__c,
                Fax,
                FirstName,
                Salutation,
                ID,
                REG_IMS_ID__c,
                LastName,
                Middle_vod__c,
                Phone,
                Specialty_1_vod__c,
                RecordTypeID,
                REG_RegID__c,
                REG_Suffix__c,
                REG_Title__c,
                Website,
                REG_Infusions__c,
                REG_of_Infusions_in_Their_Practice__c,
                REG_of_Patients_on_a_Generic_Biosimilar__c,
                REG_of_Patients_on_Biologic_Agents__c,
                REG_Competition_Speaker__c,
                REG_Regeneron_Speaker__c,
                REG_Role__c,
                Gender_vod__c,
                Preferred_Name_vod__c,
                PersonMobilePhone,
                Primary_Parent_vod__c,
                REG_Verification_Status__c,
                REG_Title_2__c,
                REG_Title_3__c,
                REG_Created_By_Profile_Name__c,
                REG_Email__c,
                REG_Expert__c,
                REG_Last_Modified_by_Profile__c,
                NPI_vod__c,
                REG_MedPro_First_Name__c,
                REG_MedPro_Last_Name__c,
                REG_MedPro_Designation__c,
                REG_DUPI_Attend_Speaker_Program__c,
                REG_DUPI_Part_of_National_Group__c,
                REG_DUPI_Expert__c,
                REG_DUPI_of_Patients_on_Biologic_Agents__c,
                REG_DERMATOLOGY_Segment__c,
                REG_DERMATOLOGY_Target__c,
                REG_Associated_Products__c,
                REG_RHEUMATOLOGY_Target__c,
                REG_RHEUMATOLOGY_Tier__c,
                REG_CARDIO_METABOLIC_Tier__c,
                REG_OPHTHALMOLOGY_Tier__c,
                REG_IMMUNOLOGY_Tier__c,
                REG_CARDIO_METABOLIC_Target__c,
                REG_OPHTHALMOLOGY_Target__c,
                REG_IMMUNOLOGY_Target__c,
                REG_RESPIRATORY_Tier__c,
                REG_PAIN_Tier__c,
                REG_ONCOLOGY_Tier__c,
                REG_TA4_Tier__c,
                REG_TA5_Tier__c,
                REG_RESPIRATORY_Target__c,
                REG_PAIN_Target__c,
                REG_ONCOLOGY_Target__c,
                REG_TA4_Target__c,
                REG_TA5_Target__c,
                REG_VEEVAEXT__c,
                REG_DERMATOLOGY_Opportunity_Ranking__c,
                EYLEA_DMS_Target__c,
                REG_VEGF_Injector__c,
                REG_NP_Target__c,
                REG_NP_Tier__c
            };
            return String.Join(",", totalValues.ToArray());
        }
    }
}
